import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Home from "./Pages/Home";
import Signup from "./Pages/Signup";
import Login from "./Pages/Login";
import MainMenu from "./components/Menu";
import Payments from "./Pages/Payments";
import "react-credit-cards/es/styles-compiled.css";
import UC from "universal-cookie";

const cookie = new UC();

class App extends React.Component {
  render() {
    return (
      <div>
        <MainMenu />
        <Switch>
          <Route exact path="/signup" component={Signup} />
          <Route exact path="/login" component={Login} />
          {cookie.get("AuthToken") && (
            <Route exact path="/home" component={Home} />
          )}

          {!cookie.get("AuthToken") && <Redirect from="/home" to="/login" />}

          <Route exact path="/payments" component={Payments} />
        </Switch>
      </div>
    );
  }
}

export default App;
