import React from 'react';
import Frame from '../components/Frame';
import {Row, Col, Card} from 'antd';
import axios from 'axios';
import '../Styles/Home.css';



class Home extends React.Component {
state= {
  users : []
}
  componentDidMount = () =>{
    axios.get('http://54.177.45.103:3001/user/all')
    .then(result=>{
      if(result.status === 200){
        this.setState({users : result.data.serverResponse})
      }
    })
    .catch(err=>{
      if(err.response.status === 404){
        alert('No user found.')
      }else{
        alert('something went wrong')
      }
    });
  }
 
  render() {
    return (
      <div>
       <Row gutter={25}>
         <Col xs={24} md={24} lg={6} xl={6}>
           <Card className="filter">

           </Card>
         </Col>
              <Col xs={24} md={24} lg={18} xl={18}>
                <div className="profiles_div">

  {this.state.users.map((user)=>(
           <Frame
       image= "https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"
       title = {user.fName}
       description = {user.lName}/>
         ))} 

              
                </div>
       
          </Col>
            </Row>
      </div>
    )
  }
}


export default Home