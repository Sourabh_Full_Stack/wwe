import React from "react";
import Cards from "react-credit-cards";
import "../Styles/Payments.css";
import { Row, Col, Card, Button } from "antd";
import { DollarCircleOutlined } from "@ant-design/icons";

class Payments extends React.Component {
  state = {
    cvc: "",
    expiry: "",
    focus: "",
    name: "",
    number: "",
  };

  handleInputFocus = (e) => {
    this.setState({ focus: e.target.name });
  };

  handleInputChange = (e) => {
    const { name, value } = e.target;

    this.setState({ [name]: value });
  };

  render() {
    return (
      <div className="main_hero_payments">
        <h1 className="payment_label">Doctor's Vivah Secure Payment</h1>
        <Row justify="center" gutter={10}>
            <Col xs={22} md={22} lg={12} xl={12}>
            <Row>
          <Col xs={22} md={22} lg={22} xl={22}>
            <Card className="payment_card">
              <Row justify="center" gutter={10}>
                <Col xs={22} md={22} lg={12} xl={12}>
                  <form>
                    <input
                      type="tel"
                      name="number"
                      placeholder="Card Number"
                      className="input"
                      onChange={this.handleInputChange}
                      onFocus={this.handleInputFocus}
                    />
                    <br />
                    <br />
                    <input
                      type="tel"
                      name="name"
                      placeholder="Name"
                      className="input"
                      onChange={this.handleInputChange}
                      onFocus={this.handleInputFocus}
                    />
                    <br />
                    <br />
                    <input
                      type="tel"
                      name="expiry"
                      placeholder="Valid Thru"
                      className="input"
                      onChange={this.handleInputChange}
                      onFocus={this.handleInputFocus}
                    />
                    <br />
                    <br />
                    <input
                      type="tel"
                      name="cvc"
                      placeholder="CVV"
                      className="input"
                      onChange={this.handleInputChange}
                      onFocus={this.handleInputFocus}
                    />
                  </form>
                </Col>
                <Col xs={22} md={22} lg={10} xl={10}>
                  <Cards
                    cvc={this.state.cvc}
                    expiry={this.state.expiry}
                    focused={this.state.focus}
                    name={this.state.name}
                    number={this.state.number}
                  />
                </Col>
                <label className="axis">
                  Powered by Axis International - Advancing Security Worldwide.
                </label>
                <br />
                <br />
                <Button className="buttonP" type="primary" icon={<DollarCircleOutlined />}>
                  Pay
                </Button>
              </Row>
            </Card>
          </Col>
        </Row>
            </Col>
            <Col xs={22} md={22} lg={10} xl={10}>
            <img className="lock" src="https://static.wixstatic.com/media/727358_877ada4c48444d6f9586e377b230bcfa~mv2.gif" />
            </Col>
        </Row>
       
       
      </div>
    );
  }
}

export default Payments;
