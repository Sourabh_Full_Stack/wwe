import React from "react";
import "../Styles/Signup.css";
import axios from "axios";
import cogoToast from 'cogo-toast';
import { Row, Col, Card, Input, Button } from "antd";

class Signup extends React.Component {
  state = {
    fname: null,
    lname: null,
    email: null,
    password: null,
  };

  handleInputChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = () => {
    axios.post('http://54.177.45.103:3001/user/signup', this.state)
    .then(result=>{
      if(result.status === 201){
        cogoToast.success('You are now successfully registered. Yay!');
       
      }
      this.props.history.push('/login')
    })
    .catch(err=>{
      if(err.response.status === 500){
        cogoToast.error('Something went wrong, try again.')
      }
      else if(err.response.status === 409){
        cogoToast.warn('An account with this email already exists. Try registering with a new email address.')
        
      }
    });
  };

  render() {
    return (
      <div>
        <div className="main_signup_div">
          <div className="signup_frame">
            <Row justify={"center"} gutter={25}>
              <Col xs={22} ml={22} lg={10} xl={10}>
                <img
                  className="imageS"
                  src="https://esceneric.com/wp-content/uploads/2015/07/digital-marketing-esceneric.png" alt="company"
                />
              </Col>
              <Col xs={22} ml={22} lg={10} xl={10}>
                <Card className="co_cardS">
                  <h1 className="register_text">REGISTER</h1>
                  <br />
                  <br />
                  <br />
                  <br />
                  <label className="signup_text">
                    Enter a world of impossibilities.
                  </label>
                  <br />
                  <br />
                  <Input
                    name="fName"
                    onChange={this.handleInputChange}
                    placeholder="First Name"
                  />
                  <br />
                  <br />
                  <Input
                    name="lName"
                    onChange={this.handleInputChange}
                    placeholder="Last Name"
                  />
                  <br />
                  <br />
                  <Input
                    name="email"
                    onChange={this.handleInputChange}
                    placeholder="Email"
                  />
                  <br />
                  <br />
                  <Input.Password
                    name="password"
                    onChange={this.handleInputChange}
                    placeholder="Password"
                  />
                  <br />
                  <br />
                  <br />
                  <Button
                    onClick={this.handleSubmit}
                    className="buttonS"
                    type="primary"
                  >
                    Signup
                  </Button>
                </Card>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    );
  }
}

export default Signup;
