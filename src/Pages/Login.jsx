import React from "react";
import "../Styles/Login.css";
import axios from "axios";
import UC from 'universal-cookie';
import { Row, Col, Card, Input, Button } from "antd";
import cogoToast from "cogo-toast";

const cookies = new UC();

class Login extends React.Component {
  state = {
    email: null,
    password: null
  };

  handleInputChnageL = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmitL = () => {
    axios
      .post("http://54.177.45.103:3001/user/login", this.state)
      .then((result) => {
        if (result.status === 200)
        {
          
          cogoToast.success("Login success.");
        }
        this.setState({isLoggedIn : true});
        cookies.set('AuthToken', result.data.AuthToken);
       
        console.log(result);
        

      })
      .catch((error) => {
       if(error.response.status === 404){
          cogoToast.warn("No account with this email id already exists.");
        }
        else if(error.response.status === 401){
          cogoToast.warn("Invalid Username or Password, Authentication failed.")
        }
        else {
          cogoToast.error("Something went wrong, try again.");
        }
          
        
      });
  };

  render() {
    return (
      <div>
        <div className="main_login_div">
          <div className="login_frame">
            <Row justify={"center"} gutter={25}>
              <Col xs={22} ml={22} lg={10} xl={10}>
                <img
                  className="image"
                  src="https://chicagopolicyreview.org/wp-content/uploads/2016/06/iStock_000072549603_Medium.jpg"
                  alt="company"
                />
              </Col>
              <Col xs={22} ml={22} lg={10} xl={10}>
                <Card className="co_card">
                  <h1 className="signin_text"> LOGIN</h1>
                  <br />
                  <br />
                  <br />
                  <br />
                  <label className="login_text">
                    Welcome Back! Let's begin
                  </label>
                  <br />
                  <br />
                  <Input
                    onChange={this.handleInputChnageL}
                    name="email"
                    placeholder="Email"
                  />
                  <br />
                  <br />
                  <Input.Password
                    onChange={this.handleInputChnageL}
                    name="password"
                    placeholder="Password"
                  />
                  <br />
                  <br />
                  <br />
                  <Button
                    onClick={this.handleSubmitL}
                    className="button"
                    type="primary"
                  >
                    Login
                  </Button>
                </Card>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
