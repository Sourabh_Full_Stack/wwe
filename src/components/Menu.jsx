import React from 'react';
import {Menu} from 'antd';
import 'antd/dist/antd.css';
import {HomeOutlined, UserAddOutlined, LoginOutlined, LogoutOutlined} from '@ant-design/icons';
import {withRouter} from 'react-router-dom';

class MainMenu extends React.Component {
  state = {
    current : 'home',
    isLoggedIn : false
  }

  componentDidUpdate = () => {
    if(this.state.current === 'home'){
      this.props.history.push('/home')
    }
  }


  handleClick = (e) =>{
     this.setState({current : e.key}) 
    if(e.key === 'home'){
      this.props.history.push('/home')
    }
    if(e.key === 'signup'){
      this.props.history.push('/signup')
    }
    if(e.key === 'login'){
      this.props.history.push('/login')
    }
    if(e.key === 'logout'){
      this.props.history.push('/login')
    }
  }

  render() {
    return (
      <div>
               <Menu
        selectedKeys={[this.state.current]}
          mode= "horizontal"
          theme="dark"
          onClick = {this.handleClick}
          
        >
          <Menu.Item key="home" icon={<HomeOutlined />}>
            Home
          </Menu.Item>
          <Menu.Item key="signup" icon={<UserAddOutlined />}>
            Signup
          </Menu.Item>
          <Menu.Item key="login" icon={<LoginOutlined />}>
            Login
          </Menu.Item>
          <Menu.Item key="logout" icon={<LogoutOutlined />}>
            Logout
          </Menu.Item>
        
        </Menu>
      </div>
    )
  }
}


export default withRouter(MainMenu)